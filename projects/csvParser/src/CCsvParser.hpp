#ifndef CSVPARSER_HPP__8uOvpFHbKvSjKrUENADkg2ALC
#define CSVPARSER_HPP__8uOvpFHbKvSjKrUENADkg2ALC
//------------------------------------------------------------------------------
#include <vector>
#include <string>
#include <fstream>
//==============================================================================
class CCsvParser
{
    public:
        CCsvParser ();
        bool ParseInput ( std::istream & is );
        void Print () const;
        std::vector<std::vector<std::string> > const & GetVector () const;

    private:
        std::vector<std::vector<std::string> > v_;
        int row_;
        int col_;

        int curState_;

        void InitNewCell ();
        void InitNewLine ();
        void StoreChar ( char c );
        bool LastLineEmpty () const;

        bool DoState ( char c );
        bool DoState0 ( char c );
        bool DoState1 ( char c );
        bool DoState2 ( char c );
        bool DoState3 ( char c );
        bool DoState4 ( char c );
        bool DoState5 ( char c );
};
//==============================================================================
#endif // CSVPARSER_HPP
