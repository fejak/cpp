#include "CCsvParser.hpp"

#include <iostream> //  std::cout, std::cin, std::endl
#include <cstdlib>  //  EXIT_SUCCESS, EXIT_FAILURE
#include <fstream>  //  std::ifstream
//------------------------------------------------------------------------------
int main ( int argc, char * argv [] )
{
    CCsvParser c;

    if ( argc > 1 )
    {
        std::ifstream inFile ( argv [1] );
        if ( inFile ) // if the file was correctly opened
        {
            c . ParseInput ( inFile );
        }
        else
        {
            std::cout << "Error opening the input file." << std::endl;
            return EXIT_FAILURE;
        }
    }
    else // no file argument entered - read from stdin
    {
        c . ParseInput ( std::cin );
    }

    c . Print ();

    return EXIT_SUCCESS;
}
