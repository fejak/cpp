#include "CCsvParser.hpp"

#include <iostream>
#include <cstdlib>
#include <vector>
#include <string>
#include <fstream>
//------------------------------------------------------------------------------
CCsvParser::CCsvParser ()
    : row_ ( -1 ), col_ ( -1 ), curState_ ( 0 )
{}
//------------------------------------------------------------------------------
void CCsvParser::InitNewCell ()
{
    v_ [row_] . push_back ( std::string () );
    ++ col_;
}
//------------------------------------------------------------------------------
void CCsvParser::InitNewLine ()
{
    v_ . push_back ( std::vector<std::string> () );
    ++ row_;
    col_ = -1;
    InitNewCell ();
}
//------------------------------------------------------------------------------
void CCsvParser::StoreChar ( char c )
{
    v_ [row_][col_] . push_back ( c );
}
//------------------------------------------------------------------------------
bool CCsvParser::LastLineEmpty () const
{
    for ( size_t i = 0; i < v_ . back () . size (); ++i )
        if ( v_ . back () [i] != "" )
            return false;
    return true;
}
//------------------------------------------------------------------------------
std::vector<std::vector<std::string> > const & CCsvParser::GetVector () const
{
    return v_;
}
//------------------------------------------------------------------------------
void CCsvParser::Print () const
{
    for ( size_t i = 0; i < v_ . size (); ++i )
    {
        for ( size_t j = 0; j < v_ [i] . size (); ++j )
        {
            std::cout << v_ [i][j];

            if ( j < v_ [i] . size () - 1 )
                std::cout << " | ";
        }

        if ( i < v_ . size () - 1 )
            std::cout << '\n';
    }

    std::cout << std::flush;
}
//------------------------------------------------------------------------------
bool CCsvParser::DoState ( char c )
{
    switch ( curState_ )
    {
        case 0: return DoState0 ( c );
        case 1: return DoState1 ( c );
        case 2: return DoState2 ( c );
        case 3: return DoState3 ( c );
        case 4: return DoState4 ( c );
        case 5: return DoState5 ( c );
        default: return false; // ???
    }
}
//------------------------------------------------------------------------------
bool CCsvParser::ParseInput ( std::istream & is )
{
    char c;

    InitNewLine ();

    while ( true )
    {
        is . get ( c );
        if ( is . eof () )
            break;
        if ( ! DoState ( c ) )
            return false;       // error state
    }

    // if the last line is empty (should be), remove it
    if ( LastLineEmpty () )
        v_ . pop_back ();

    return true;
}
//------------------------------------------------------------------------------
// beginning of the first cell total
bool CCsvParser::DoState0 ( char c )
{
    switch ( c )
    {
        case '"':
            curState_ = 1;
            return true;
        case ',':
            InitNewCell ();
            curState_ = 2;
            return true;
        case '\n':
            curState_ = 0;
            return true;
        default:
            StoreChar ( c );
            curState_ = 3;
            return true;
    }
}
//------------------------------------------------------------------------------
// inside quoted cell - no quote char preceded
bool CCsvParser::DoState1 ( char c )
{
    // inside quoted cell - no quote char preceded
    switch ( c )
    {
        case '"':
            curState_ = 4;
            return true;
        case ',':
            StoreChar ( c );
            curState_ = 1;
            return true;
        case '\n':
            StoreChar ( c );
            curState_ = 1;
            return true;
        default:
            StoreChar ( c );
            curState_ = 1;
            return true;
    }
}
//------------------------------------------------------------------------------
// beginning of a new cell (nonfirst)
bool CCsvParser::DoState2 ( char c )
{
    switch ( c )
    {
        case '"':
            curState_ = 1;
            return true;
        case ',':
            InitNewCell ();
            curState_ = 2;
            return true;
        case '\n':
            InitNewLine ();
            curState_ = 2;
            return true;
        default:
            StoreChar ( c );
            curState_ = 3;
            return true;
    }
}
//------------------------------------------------------------------------------
// inside non-quoted cell
bool CCsvParser::DoState3 ( char c )
{
    switch ( c )
    {
        case '"':
            return false;
        case ',':
            InitNewCell ();
            curState_ = 2;
            return true;
        case '\n':
            InitNewLine ();
            curState_ = 2;
            return true;
        default:
            StoreChar ( c );
            curState_ = 3;
            return true;
    }
}
//------------------------------------------------------------------------------
// inside quoted cell - one quote char preceded
bool CCsvParser::DoState4 ( char c )
{
    switch ( c )
    {
        case '"':
            StoreChar ( c );
            curState_ = 5;
            return true;
        case ',':
            InitNewCell ();
            curState_ = 2;
            return true;
        case '\n':
            InitNewLine ();
            curState_ = 2;
            return true;
        default:
            return false;
    }
}
//------------------------------------------------------------------------------
// inside quoted cell - two quote chars preceded
bool CCsvParser::DoState5 ( char c )
{
    switch ( c )
    {
        case '"':
            curState_ = 4;
            return true;
        case ',':
            StoreChar ( c );
            curState_ = 1;
            return true;
        case '\n':
            StoreChar ( c );
            curState_ = 1;
            return true;
        default:
            StoreChar ( c );
            curState_ = 1;
            return true;
    }
}
