# CSV Parser

This programs takes a CSV file as an input and parses it into an internal representation (2D vector) which is easy to work with further on.

## Compile & run

* Compile
  * `make all`
* Run
  * `make run`
* Run tests
  * `make test1`
  * `make test2`
* Delete files
  * `make clean`
    * Deletes the object files + the dependency files; keeps the exe file.
  * `make purge`
    * Deletes everything except the source files, the CSV test files and the readme file (returns the project to its initial state).
