#include "CFrontEnd.hpp"
#include "colors.hpp"

#include <cstdlib> // EXIT_SUCCESS
#include <ncurses.h>

#include <iostream> // std::cout 
//------------------------------------------------------------------------------
void initNcurses ()
{
  initscr ();              // init ncurses
  cbreak ();               // disable line buffering
  noecho ();               // disable echo
  keypad ( stdscr, TRUE ); // enable special keystrokes (backspace, delete...)
  curs_set ( 0 );          // hide cursor
  set_escdelay ( 0 );      // escape key delay
  clear ();
}
//------------------------------------------------------------------------------
int main ()
{
  initNcurses ();
  initColors ();   // from colors.hpp

  CFrontEnd f;

  endwin ();

  std::cout << "score: " << f . GetScore () << std::endl;

  return EXIT_SUCCESS;
}
