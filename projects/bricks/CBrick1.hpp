#ifndef HKK4y5GASPqH5LCkDHb17sGKi39h55m9DPoUH3w7IIWFADSrWQ
#define HKK4y5GASPqH5LCkDHb17sGKi39h55m9DPoUH3w7IIWFADSrWQ
//------------------------------------------------------------------------------
#include "CLevel.hpp"
#include "CBrick.hpp"

#include "colors.hpp"

#include <ncurses.h> // COLOR_RED
//------------------------------------------------------------------------------
/*  CBrick1 shape:
 *  
 *  XXXXXXXX
 *
 *  XX
 *  XX
 *  XX
 *  XX
 * 
 */
//------------------------------------------------------------------------------
class CBrick1 : public CBrick
{
  public:
    //--------------------------------------------------------------------------
    CBrick1 ( CLevel const & level )
      : CBrick ( level, 4, RED, 2 )
    {
      // define brick shape - just define default basic shape
      m_BrickArray [1][0] = 1;
      m_BrickArray [1][1] = 1;
      m_BrickArray [1][2] = 1;
      m_BrickArray [1][3] = 1;
    }
    //--------------------------------------------------------------------------
  private:
    //--------------------------------------------------------------------------
    // m_Y, m_X inherited
};
//------------------------------------------------------------------------------
#endif
