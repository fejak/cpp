//------------------------------------------------------------------------------
#include "CFrontEnd.hpp"
#include "CLevel.hpp"

#include "CBrick1.hpp"
#include "CBrick2.hpp"
#include "CBrick3.hpp"

#include <unistd.h>     // usleep ()
#include <iostream>     // std::cout ()

#include <cstdlib>     // srand, rand
#include <ctime>       // time

#include <ncurses.h>
//------------------------------------------------------------------------------
static int const TICK_TIME      = 5000;
static int const DESCENT_LIMIT  = 200000 / TICK_TIME;
static int const FASTER_LIMIT   = DESCENT_LIMIT / 4;    // down arrow key
static int const FALL_LIMIT1    = 1;//3000 / TICK_TIME; // column
static int const FALL_LIMIT2    = 1;//4000 / TICK_TIME; // no column
//------------------------------------------------------------------------------
CFrontEnd::CFrontEnd ()
  : m_Level ( NULL ), m_Brick ( NULL ), m_Score ( 0 )
{
  int screenHeight;
  int screenWidth;
  
  getmaxyx ( stdscr, screenHeight, screenWidth );

  int yPos = screenHeight / 2 - LEVEL_HEIGHT / 2;
  int xPos = screenWidth  / 2 - LEVEL_WIDTH  / 2 - 1; // -1 ... ?????????

  m_Level = new CLevel ( yPos, xPos, LEVEL_HEIGHT, LEVEL_WIDTH );

  ControlLoop ();
}
//------------------------------------------------------------------------------
CFrontEnd::~CFrontEnd ()
{
  delete m_Level;
  if ( m_Brick != NULL )    // needed ???
    delete m_Brick;
}
//------------------------------------------------------------------------------
void CFrontEnd::SwitchFlag ( bool & flag, int key, char c ) const
{
  if ( key == c || key == c - 32 ) // lowercase + uppercase
    flag = ! flag;
}
//------------------------------------------------------------------------------
void CFrontEnd::ControlLoop ()
{
  bool drawColumn = true;

  int limitCnt = 0;
  int limit = DESCENT_LIMIT;    // normal delay between brick descend
  
  bool falling = false; // block any input if brick is falling
  bool drawShadow = true;
  bool paused = false;
  bool drawGrid = false;

  srand ( time ( NULL ) );

  while ( true )
  {
    if ( m_Brick == NULL )
    {
      SpawnBrick ();

      if ( ! m_Brick -> CheckPosition ( * m_Level ) )
        return;
    }

    //int retval = ReadKey ();
    timeout ( 0 );
    int key = getch ();
   
    if ( key == 27 ) // escape
    {
      std::cout << m_Score << std::endl;
      return;   // return - end loop => end program
    }

    SwitchFlag ( drawGrid, key, 'g' );
    SwitchFlag ( drawShadow, key, 's' );
    SwitchFlag ( drawColumn, key, 'c' );

    if ( paused && key != 'p' && key != 'P' )
    {
      RefreshScreen ( falling, drawShadow, drawColumn, drawGrid );
      continue;
    }

    switch ( key )
    {
      case 'p':
      case 'P':
        paused = ! paused;
        break;

      case KEY_LEFT:
        if ( ! falling )
          m_Brick -> MoveLeft ( * m_Level );
        break;
        
      case KEY_RIGHT:
        if ( ! falling )
          m_Brick -> MoveRight ( * m_Level );
        break;
        
      case KEY_UP:
        if ( ! falling )
          m_Brick -> Rotate ( * m_Level, false );
        break;
        
      case KEY_DOWN:
        if ( ! falling )
          limit = FASTER_LIMIT;
        break;
        
      case ' ':
  //      m_Brick -> Fall ( * m_Level );
  
        if ( drawColumn ) // column must be faster (more "dynamic")
          limit = FALL_LIMIT1;
        else
          limit = FALL_LIMIT2;
  
        falling = true;
        break;
    }

    if ( limitCnt >= limit )
    {
      if ( ! m_Brick -> MoveDown ( * m_Level ) )    // if brick can't descend anymore
      {
        m_Brick -> Write ( * m_Level );    // write it into the level
        delete m_Brick;
        m_Brick = NULL;
        
        falling = false;
        
        m_Score += AddScore ( m_Level -> CheckLevel () );
        
        limit = DESCENT_LIMIT;
      }
      
      if ( limit == FASTER_LIMIT )
        limit = DESCENT_LIMIT;
      
      limitCnt = 0;
    }

    RefreshScreen ( falling, drawShadow, drawColumn, drawGrid );
    
    ++ limitCnt;
  }
}
//------------------------------------------------------------------------------
void CFrontEnd::RefreshScreen ( bool falling, bool drawShadow,
                                bool drawColumn, bool drawGrid ) const
{
  erase ();     // clear () flickers ... erase () does not
  //clear ();
  
  Draw ( drawShadow, drawGrid ); // draw the whole main screen

  // draw column
  if ( falling && drawColumn )
    m_Brick -> DrawColumn ( * m_Level );
  
  refresh ();

  usleep ( TICK_TIME );
}
//------------------------------------------------------------------------------
void CFrontEnd::Draw ( bool drawShadow, bool drawGrid ) const
{
  m_Level -> Draw ( drawGrid );
  if ( m_Brick != NULL )
  {
   // m_Brick -> Refresh ( * m_Level ); 
    if ( drawShadow )
      m_Brick -> DrawShadow ( * m_Level );
    
    m_Brick -> DrawBrick (); 
  }
}
//------------------------------------------------------------------------------
/*int CFrontEnd::ReadKey () const
{
  timeout ( 0 ); // non-blocking read with timeout 100 ms ( ??? )
  int key = getch ();
  int retval = key;
  switch ( key )
  {
    case '\t':
      SwitchWindow ();
      return 0;
      break;
    default:
      retval += m_WinMenu     -> ReadKey ( key );
      retval += m_WinFileList -> ReadKey ( 0 );   // no action
      retval += m_WinViewer   -> ReadKey ( key );
      break;
  }
  return retval;
}*/
//------------------------------------------------------------------------------
void CFrontEnd::SpawnBrick ()
{
  int random = rand () % 3 + 1;
  //int random = 1;

  switch ( random )
  {
    case 1:
    //default:
      m_Brick = new CBrick1 ( * m_Level );
      break;

    case 2:
      m_Brick = new CBrick2 ( * m_Level );
      break;
      
    case 3:
      m_Brick = new CBrick3 ( * m_Level );
      break;
  }
}
//------------------------------------------------------------------------------
int CFrontEnd::AddScore ( int rowsDeleted ) const
{
  switch ( rowsDeleted )
  {
    case 1: return 40;
    case 2: return 100;
    case 3: return 300;
    case 4: return 1200;
  }
  return 0;
}
//------------------------------------------------------------------------------
int CFrontEnd::GetScore () const
{
  return m_Score;
}
