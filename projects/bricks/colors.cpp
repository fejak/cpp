//------------------------------------------------------------------------------
#include "colors.hpp"

#include <ncurses.h>
//------------------------------------------------------------------------------
void initColors ()
{
  start_color ();
  
  // fg colors
  init_pair ( BLACK,   COLOR_BLACK,   COLOR_BLACK );
  init_pair ( RED,     COLOR_RED,     COLOR_BLACK );
  init_pair ( GREEN,   COLOR_GREEN,   COLOR_BLACK );
  init_pair ( YELLOW,  COLOR_YELLOW,  COLOR_BLACK );
  init_pair ( BLUE,    COLOR_BLUE,    COLOR_BLACK );
  init_pair ( MAGENTA, COLOR_MAGENTA, COLOR_BLACK );
  init_pair ( CYAN,    COLOR_CYAN,    COLOR_BLACK );
  init_pair ( WHITE,   COLOR_WHITE,   COLOR_BLACK );
  
  // bg colors
  init_pair ( BLACK + 50,   COLOR_BLACK, COLOR_BLACK );
  init_pair ( RED + 50,     COLOR_BLACK, COLOR_RED );
  init_pair ( GREEN + 50,   COLOR_BLACK, COLOR_GREEN );
  init_pair ( YELLOW + 50,  COLOR_BLACK, COLOR_YELLOW );
  init_pair ( BLUE + 50,    COLOR_BLACK, COLOR_BLUE );
  init_pair ( MAGENTA + 50, COLOR_BLACK, COLOR_MAGENTA );
  init_pair ( CYAN + 50,    COLOR_BLACK, COLOR_CYAN );
  init_pair ( WHITE + 50,   COLOR_BLACK, COLOR_WHITE );
}
//------------------------------------------------------------------------------
void activateFgColor ( int color )
{
  attron ( COLOR_PAIR ( color ) );
}
//------------------------------------------------------------------------------
void deactivateFgColor ( int color )
{
  attroff ( COLOR_PAIR ( color ) );
}
//------------------------------------------------------------------------------
void activateBgColor ( int color )
{
  attron ( COLOR_PAIR ( color + 50 ) );
}
//------------------------------------------------------------------------------
void deactivateBgColor ( int color )
{
  attroff ( COLOR_PAIR ( color + 50 ) );
}
//------------------------------------------------------------------------------
void dimColor ()
{
  attron ( A_DIM );
}
//------------------------------------------------------------------------------
