#ifndef PzmNedsh5bP3WT1EsVxcv1m7g6dZK4l41RKI2VQS0Uym999Jpt
#define PzmNedsh5bP3WT1EsVxcv1m7g6dZK4l41RKI2VQS0Uym999Jpt
//------------------------------------------------------------------------------
#include "CLevel.hpp"
//------------------------------------------------------------------------------
class CBrick
{
  public:
    //--------------------------------------------------------------------------
                  CBrick            ( CLevel const & level,
                                      int brickSize,
                                      int color,
                                      int rotations );
    //--------------------------------------------------------------------------
    virtual       ~CBrick           ();
    //--------------------------------------------------------------------------
    void          MoveLeft          ( CLevel const & level );
    //--------------------------------------------------------------------------
    void          MoveRight         ( CLevel const & level );
    //--------------------------------------------------------------------------
    bool          MoveDown          ( CLevel const & level );
    //--------------------------------------------------------------------------
    void          DrawBrick         () const;
    //--------------------------------------------------------------------------
    void          DrawShadow        ( CLevel const & level ) const;
    //--------------------------------------------------------------------------
    void          Rotate            ( CLevel const & level, bool direction );
    //--------------------------------------------------------------------------
    void          PerformRotation   ( bool direction );
    //--------------------------------------------------------------------------
    void          Write             ( CLevel & level ) const;
    //--------------------------------------------------------------------------
    void          Fall              ( CLevel const & level );
    //--------------------------------------------------------------------------
    bool          CheckPosition     ( CLevel const & level ) const;             // overloaded function
    //--------------------------------------------------------------------------
    void          DrawColumn        ( CLevel const & level ) const;
    //--------------------------------------------------------------------------
  protected:
    //--------------------------------------------------------------------------
    void          DrawShape         ( int x,
                                      int y,
                                      int c ) const;
    //--------------------------------------------------------------------------
    bool          CheckPosition     ( CLevel const & level,                     // overloaded function
                                      int y,
                                      int x ) const;
    //--------------------------------------------------------------------------
    int const     m_BrickSize;
    int           m_YPos;    // positions relative to the level ???
    int           m_XPos;
    int const     m_Color;
    int const     m_Rotations; // number of available rotations
    int           m_CurrRotation;
    int        ** m_BrickArray;
    int        ** m_BrickArrayBackup; // backup for rotation
};
//------------------------------------------------------------------------------
# endif
