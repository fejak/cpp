#ifndef YfjQ0X1igrfxZHujsESQfPBhnj054F4yRBhPceodNnAnFOZDQR
#define YfjQ0X1igrfxZHujsESQfPBhnj054F4yRBhPceodNnAnFOZDQR
//------------------------------------------------------------------------------
class CLevel
{
  public:
    //--------------------------------------------------------------------------
                  CLevel                ( int yPos,
                                          int xPos,
                                          int height,
                                          int width );
    //--------------------------------------------------------------------------
                  ~CLevel               ();
    //--------------------------------------------------------------------------
    bool          PosOK                 ( int y,
                                          int x ) const;
    //--------------------------------------------------------------------------
    CLevel &      WriteAt               ( int y,
                                          int x,
                                          int color );
    //--------------------------------------------------------------------------
    int           GetHeight             () const;
    //--------------------------------------------------------------------------
    int           GetWidth              () const;
    //--------------------------------------------------------------------------
    int           GetYOfs               () const;
    //--------------------------------------------------------------------------
    int           GetXOfs               () const;
    //--------------------------------------------------------------------------
    void          Draw                  ( bool drawGrid ) const;
    //--------------------------------------------------------------------------
    int           CheckLevel            (); // returns number of rows deleted (max 4)
    //--------------------------------------------------------------------------
  private:
    //--------------------------------------------------------------------------
    bool          EmptyAt               ( int y,
                                          int x ) const;
    //--------------------------------------------------------------------------
    void          DrawBorder            () const;
    //--------------------------------------------------------------------------
    void          DrawVerticalLine      ( int yPos,
                                          int xPos,
                                          int length,
                                          int c ) const;
    //--------------------------------------------------------------------------
    void          DrawHorizontalLine    ( int yPos,
                                          int xPos,
                                          int length,
                                          int c ) const;
    //--------------------------------------------------------------------------
    void          DrawLevel             ( bool drawGrid ) const;
    //--------------------------------------------------------------------------
    bool          LineFull              ( int lineNr ) const;
    //--------------------------------------------------------------------------
    void          ShiftLevel            ( int lineNr );
    //--------------------------------------------------------------------------
    int m_YOfs;       // y-offset of the main game window
    int m_XOfs;       // x-offset of the main game window
    int m_Height;     // height of the main game window
    int m_Width;      // width of the main game window
    int ** m_Array;   // data array of the main game window (brick positions)
};
//------------------------------------------------------------------------------
#endif
