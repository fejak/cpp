#ifndef pIgEHABRqGlsUxmysaCRNtUmHoJJBByLb6Nfllx2eCk0nKJhyP
#define pIgEHABRqGlsUxmysaCRNtUmHoJJBByLb6Nfllx2eCk0nKJhyP
//------------------------------------------------------------------------------
#include "CLevel.hpp"
#include "CBrick.hpp"

#include "colors.hpp"

#include <ncurses.h> // COLOR_RED
//------------------------------------------------------------------------------
/*  CBrick1 shape:
 *  
 *    XX
 *  XXXXXX
 *    XX
 * 
 */
//------------------------------------------------------------------------------
class CBrick2 : public CBrick
{
  public:
    //--------------------------------------------------------------------------
    CBrick2 ( CLevel const & level )
      : CBrick ( level, 3, GREEN, 1 )
    {
      // define brick shape - just define default basic shape
      m_BrickArray [0][1] = 1;
      m_BrickArray [1][0] = 1;
      m_BrickArray [1][1] = 1;
      m_BrickArray [1][2] = 1;
      m_BrickArray [2][1] = 1;
    }
    //--------------------------------------------------------------------------
  private:
    //--------------------------------------------------------------------------
    // m_Y, m_X inherited
};
//------------------------------------------------------------------------------
#endif
