#ifndef Z3HR5wfv45CVBow2R6fzz7qdpQMXVjFT770HYDsOS7oJHFrSx2
#define Z3HR5wfv45CVBow2R6fzz7qdpQMXVjFT770HYDsOS7oJHFrSx2
//------------------------------------------------------------------------------
#include "CLevel.hpp"
#include "CBrick.hpp"

#include "colors.hpp"

#include <ncurses.h> // COLOR_RED
//------------------------------------------------------------------------------
/*  CBrick3 shape:
 *  
 *  XXXXXXXX
 *  XX
 * 
 */
//------------------------------------------------------------------------------
class CBrick3 : public CBrick
{
  public:
    //--------------------------------------------------------------------------
    CBrick3 ( CLevel const & level )
      : CBrick ( level, 3, BLUE, 4 )
    {
      // define brick shape - just define default basic shape
      m_BrickArray [1][0] = 1;
      m_BrickArray [1][1] = 1;
      m_BrickArray [1][2] = 1;
      m_BrickArray [2][0] = 1;
    }
    //--------------------------------------------------------------------------
  private:
    //--------------------------------------------------------------------------
    // m_Y, m_X inherited
};
//------------------------------------------------------------------------------
#endif
