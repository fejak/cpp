#include "CBrick.hpp"
#include "CLevel.hpp"

#include "colors.hpp"

#include <stdexcept>

#include <curses.h> // ACS_CKBOARD
//------------------------------------------------------------------------------
CBrick::CBrick ( CLevel const & level, int brickSize, int color, int rotations )
  : m_BrickSize ( brickSize ),
    m_YPos ( level . GetYOfs () /*+ 1*/ ),
    m_XPos ( ( level . GetXOfs ()
               + level . GetWidth () / 2
               - m_BrickSize / 2 )
             - ( + level . GetWidth ()
                 - m_BrickSize / 2 ) % 2 ),
    m_Color ( color ),
    m_Rotations ( rotations ),
    m_CurrRotation ( 0 )
{
  if ( m_Rotations != 1 && m_Rotations != 2 && m_Rotations != 4 )
  {
    endwin ();
    throw std::runtime_error ( "Wrong brick rotation number!" );
  }

  // create the brick array
  m_BrickArray       = new int * [m_BrickSize];
  m_BrickArrayBackup = new int * [m_BrickSize];
  
  for ( int y = 0; y < m_BrickSize; ++y )
  {
    m_BrickArray [y]       = new int [m_BrickSize];
    m_BrickArrayBackup [y] = new int [m_BrickSize];
  }
  
  // zero out the brick array (= no shape is defined)
  for ( int y = 0; y < m_BrickSize; ++y )
  {
    for ( int x = 0; x < m_BrickSize; ++x )
    {
      m_BrickArray [y][x]       = 0;
      m_BrickArrayBackup [y][x] = 0;
    }
  }
}
//------------------------------------------------------------------------------
CBrick::~CBrick ()
{
  for ( int y = 0; y < m_BrickSize; ++y )
  {
    delete [] m_BrickArray [y];
    delete [] m_BrickArrayBackup [y];
  }
  
  delete [] m_BrickArray;
  delete [] m_BrickArrayBackup;
}
//------------------------------------------------------------------------------
void CBrick::MoveLeft ( CLevel const & level )
{
  if ( CheckPosition ( level, m_YPos, m_XPos - 2 ) )
    m_XPos -= 2;
}
//------------------------------------------------------------------------------
void CBrick::MoveRight ( CLevel const & level )
{
  if ( CheckPosition ( level, m_YPos, m_XPos + 2 ) )
    m_XPos += 2;
}
//------------------------------------------------------------------------------
bool CBrick::MoveDown ( CLevel const & level )
{
  if ( CheckPosition ( level, m_YPos + 1, m_XPos ) )
  {
    m_YPos += 1;
    return true;
  }
  
  return false;
}
//------------------------------------------------------------------------------
void CBrick::DrawBrick () const
{
  activateBgColor ( m_Color );
  DrawShape ( m_YPos, m_XPos, ' ' );
  deactivateBgColor ( m_Color );
}
//------------------------------------------------------------------------------
void CBrick::DrawShadow ( CLevel const & level ) const
{
  // find position for shadow
  int y = m_YPos;
  
  while ( true )
  {
    if ( ! CheckPosition ( level, y, m_XPos ) )
      break;
    ++ y;
  }
  
  activateFgColor ( m_Color );
  
  dimColor ();
  
  DrawShape ( y - 1, m_XPos, ACS_CKBOARD );
  
  deactivateFgColor ( m_Color );
}
//------------------------------------------------------------------------------
void CBrick::Fall ( CLevel const & level )
{
  while ( true )
  {
    if ( ! CheckPosition ( level, m_YPos, m_XPos ) )
      break;
    ++ m_YPos;
  }
  
  -- m_YPos;
}
//------------------------------------------------------------------------------
bool CBrick::CheckPosition ( CLevel const & level ) const
{
  return CheckPosition ( level, m_YPos, m_XPos );
}
//------------------------------------------------------------------------------
void CBrick::PerformRotation ( bool direction )
{
  // rotate the brick into the backup array
  for ( int y = 0; y < m_BrickSize; ++y )
  {
    for ( int x = 0; x < m_BrickSize; ++x )
    {
      if ( direction ) // clockwise
        m_BrickArrayBackup [x][m_BrickSize - y - 1] = m_BrickArray [y][x];
      else // counter-clockwise
        m_BrickArrayBackup [m_BrickSize - x - 1][y] = m_BrickArray [y][x];
    }
  }

  // switch main and backup arrays
  int ** tmp = m_BrickArray;
  m_BrickArray = m_BrickArrayBackup;
  m_BrickArrayBackup = tmp;
  tmp = NULL;
}
//------------------------------------------------------------------------------
// true ... clockwise, false ... counter-clockwise
void CBrick::Rotate ( CLevel const & level, bool direction )
{
  if ( m_Rotations == 1 ) // 1 rotation => no rotation
    return;

  // reset the rotation counter if 360 degrees achieved
  if ( m_CurrRotation == m_Rotations )
  {
    if ( m_CurrRotation == 2 )
      direction = ! direction;
    
    m_CurrRotation = 0;
  }

  PerformRotation ( direction );
  ++ m_CurrRotation;

  // if the new position is not free => rotate back
  if ( ! CheckPosition ( level, m_YPos, m_XPos ) )
  {
    PerformRotation ( ! direction );
    -- m_CurrRotation;
  }
}
//------------------------------------------------------------------------------
bool CBrick::CheckPosition ( CLevel const & level, int yPos, int xPos ) const
{
  for ( int y = 0; y < m_BrickSize; ++y )
    for ( int x = 0; x < m_BrickSize; ++x )
      for ( int m = 0; m < 2; ++m ) // horizontal multiplier
        if (    m_BrickArray [y][x] != 0
             && ! level . PosOK ( yPos + y, xPos + 2 * x + m ) )
          return false;

  return true;
}
//------------------------------------------------------------------------------
void CBrick::Write ( CLevel & level ) const
{
  for ( int y = 0; y < m_BrickSize; ++y )
    for ( int x = 0; x < m_BrickSize; ++x )
      for ( int m = 0; m < 2; ++m ) // horizontal multiplier
        if ( m_BrickArray [y][x] != 0 )
          level . WriteAt ( m_YPos + y, m_XPos + 2 * x + m, m_Color );
}
//------------------------------------------------------------------------------
void CBrick::DrawShape ( int yPos, int xPos, int c ) const
{
  for ( int y = 0; y < m_BrickSize; ++y )
    for ( int x = 0; x < m_BrickSize; ++x )
      for ( int m = 0; m < 2; ++m ) // horizontal multiplier
        if ( m_BrickArray [y][x] != 0 )
          mvaddch ( yPos + y, xPos + 2 * x + m, c );
}
//------------------------------------------------------------------------------
void CBrick::DrawColumn ( CLevel const & level ) const
{
  activateFgColor ( m_Color );
  dimColor ();
  
  int y = 0;
  
  while ( CheckPosition ( level, m_YPos + y, m_XPos ) )
  {
    dimColor ();
    DrawShape ( m_YPos + y, m_XPos, ACS_CKBOARD );
    
    ++ y;
  }
    
  deactivateFgColor ( m_Color );
}
