# Bricks

A generic Tetris clone made in C++ using the ncurses library. Unfinished but playable. Recommended to run in a fast/bitmap terminal like xterm.

## Dependencies

* The ncurses library
  * In Ubuntu install using: `sudo apt install libncurses5-dev`

## Compile & run

* Compile
  * `make all`
* Run
  * `make run`
* Delete files
  * `make clean` (deletes the object and the dependency files)
  * `make purge` (deletes everything except the source codes and the makefile)

## Controls

* **Left arrow** - Move left
* **Right arrow** - Move right
* **Up arrow** - Rotate
* **Down arrow** - Fall faster
* **Spacebar** - Fall immediately
* **G** - Turn on/off the background grid
* **S** - Turn on/off the brick shadow
* **C** - Turn on/off the "column" ("blur effect" during the brick fall)
* **P** - Pause
* **Esc** - Exit
