#ifndef NEuHpG97OEy4uaQNORbe72iIH0AhD6Ade6kAE9hYLNkPF02LOw
#define NEuHpG97OEy4uaQNORbe72iIH0AhD6Ade6kAE9hYLNkPF02LOw
//------------------------------------------------------------------------------
//#include <ncurses.h>
//------------------------------------------------------------------------------
// TODO: convert to enum
static int const BLACK   = 1;
static int const RED     = 2;
static int const GREEN   = 3;
static int const YELLOW  = 4;
static int const BLUE    = 5;
static int const MAGENTA = 6;
static int const CYAN    = 7;
static int const WHITE   = 8;

//------------------------------------------------------------------------------
void              initColors              ();
//------------------------------------------------------------------------------
void              activateFgColor         ( int color );
//------------------------------------------------------------------------------
void              deactivateFgColor       ( int color );
//------------------------------------------------------------------------------
void              activateBgColor         ( int color );
//------------------------------------------------------------------------------
void              deactivateBgColor       ( int color );
//------------------------------------------------------------------------------
void              dimColor                ();
//------------------------------------------------------------------------------
#endif
