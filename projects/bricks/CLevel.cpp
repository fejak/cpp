//------------------------------------------------------------------------------
#include "CLevel.hpp"

#include "colors.hpp"

#include <ncurses.h>
#include <cstddef> // NULL
//------------------------------------------------------------------------------
static const int COORD_EMPTY = -1; // empty coordinate
//------------------------------------------------------------------------------
CLevel::CLevel ( int yPos, int xPos, int height, int width )
  : 
    m_YOfs ( yPos ),
    m_XOfs ( xPos ),
    m_Height ( height ),
    // width must always be even in order to fit the bricks inside the level
    m_Width ( width + width % 2 ),
    m_Array ( NULL )
{
  m_Array = new int * [ m_Height ];
  
  for ( int i = 0; i < m_Height; ++i )
    m_Array [i] = new int [ m_Width ];
  
  // make array empty
  for ( int i = 0; i < m_Height; ++i )
    for ( int j = 0; j < m_Width; ++j )
      m_Array [i][j] = COORD_EMPTY;
}
//------------------------------------------------------------------------------
CLevel::~CLevel ()
{
  for ( int i = 0; i < m_Height; ++i )
    delete [] m_Array [i];
    
  delete [] m_Array;
}
//------------------------------------------------------------------------------
// checking whether position is empty at given coordinates
bool CLevel::EmptyAt ( int y, int x ) const
{
  return m_Array [y][x] == COORD_EMPTY;
}
//------------------------------------------------------------------------------
// checking whether position is OK at given coordinates
// "position OK" = a brick can move there
bool CLevel::PosOK ( int y, int x ) const
{
  return  y >= m_YOfs
          && x >= m_XOfs
          && y < m_YOfs + m_Height
          && x < m_XOfs + m_Width
          && EmptyAt ( y - m_YOfs, x - m_XOfs );
}
//------------------------------------------------------------------------------
CLevel & CLevel::WriteAt ( int y, int x, int color )
{
  m_Array [y - m_YOfs][x - m_XOfs] = color;
  return * this;
}
//------------------------------------------------------------------------------
int CLevel::GetHeight () const
{
  return m_Height;
}
//------------------------------------------------------------------------------
int CLevel::GetWidth () const
{
  return m_Width;
}
//------------------------------------------------------------------------------
int CLevel::GetYOfs () const
{
  return m_YOfs;
}
//------------------------------------------------------------------------------
int CLevel::GetXOfs () const
{
  return m_XOfs;
}
//------------------------------------------------------------------------------
void CLevel::Draw ( bool drawGrid ) const
{
  DrawBorder ();
  DrawLevel ( drawGrid );
}
//------------------------------------------------------------------------------
void CLevel::DrawBorder () const
{
  activateFgColor ( WHITE );
  
  // left vertical
  DrawVerticalLine ( m_YOfs, m_XOfs - 1, m_Height, ACS_VLINE );
  // right vertical
  DrawVerticalLine ( m_YOfs, m_XOfs + m_Width, m_Height, ACS_VLINE );
  // top horizontal
  DrawHorizontalLine ( m_YOfs - 1, m_XOfs, m_Width, ACS_HLINE );
  // bottom horizontal
  DrawHorizontalLine ( m_YOfs + m_Height, m_XOfs, m_Width, ACS_HLINE );

  // draw corners
  mvaddch ( m_YOfs - 1, m_XOfs - 1, ACS_ULCORNER );
  mvaddch ( m_YOfs - 1, m_XOfs + m_Width, ACS_URCORNER );
  mvaddch ( m_YOfs + m_Height, m_XOfs - 1, ACS_LLCORNER );
  mvaddch ( m_YOfs + m_Height, m_XOfs + m_Width, ACS_LRCORNER );

  deactivateFgColor ( WHITE );
}
//------------------------------------------------------------------------------
void CLevel::DrawVerticalLine ( int yPos, int xPos, int length, int c ) const
{
  for ( int i = 0; i < length; ++i )
    mvaddch ( yPos + i, xPos, c );
}
//------------------------------------------------------------------------------
void CLevel::DrawHorizontalLine ( int yPos, int xPos, int length, int c ) const
{
  for ( int i = 0; i < length; ++i )
    mvaddch ( yPos, xPos + i, c );
}
//------------------------------------------------------------------------------
void CLevel::DrawLevel ( bool drawGrid ) const
{
  for ( int y = 0; y < m_Height; ++y )
    for ( int x = 0; x < m_Width; ++x )
      if ( ! EmptyAt ( y, x ) )
      {
        activateBgColor ( m_Array [y][x] );
        mvaddch ( y + m_YOfs, x + m_XOfs, ' ' );
        deactivateBgColor ( m_Array [y][x] );
      }
      else if ( drawGrid )
      {
        if ( x % 2 == 0 )
          mvaddch ( y + m_YOfs, x + m_XOfs, '.' );
      }
}
//------------------------------------------------------------------------------
// checking for full lines
int CLevel::CheckLevel ()
{
  int rowsDeleted = 0;
  
  // scan the level vertically
  for ( int y = 0; y < m_Height; ++y )
  {
    if ( LineFull ( y ) )
    {
      ShiftLevel ( y );
      ++ rowsDeleted;
    }
  }
  
  return rowsDeleted;
}
//------------------------------------------------------------------------------
// returns true if given line is full, false if it's not full
bool CLevel::LineFull ( int lineNr ) const
{
  // scan the line horizontally
  for ( int x = 0; x < m_Width; ++x )
  {
    if ( EmptyAt ( lineNr, x ) )
      return false;
  }
  return true;
}
//------------------------------------------------------------------------------
// shifts the whole level downwards until the given line number
void CLevel::ShiftLevel ( int lineNr )
{
  for ( int y = lineNr; y > 0; --y )
    for ( int x = 0; x < m_Width; ++x )
      m_Array [y][x] = m_Array [y - 1][x];
}
//------------------------------------------------------------------------------
