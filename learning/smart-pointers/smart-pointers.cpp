#include <iostream>     // std::cout, std::endl
#include <memory>       // std::unique_ptr
#include <utility>      // std::move

int main ()
{
    // C++11
    // Initializes the unique_ptr to an array of 5 integers
    std::unique_ptr<int[]> ptr1 ( new int [5] );
    
    ptr1 [0] = 123;
    
    std::cout << ptr1 [0] << std::endl;

    // C++14
    std::unique_ptr<float> ptr2 = std::make_unique<float>(15);

    // error - the constructor is explicit
    // https://stackoverflow.com/questions/37275566
    // https://stackoverflow.com/questions/47709572
    // std::unique_ptr<float> ptr4 = new float (5);
    // ok
    std::unique_ptr<float> ptr4 ( new float (5) );

    // unique_ptr => ptr2 becomes nullptr
    std::unique_ptr<float> ptr3 = std::move ( ptr2 );

    std::cout << ( ptr2 == nullptr ) << std::endl;

    return 0;
}
