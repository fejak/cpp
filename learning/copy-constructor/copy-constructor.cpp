#include <iostream>
#include <vector>
//------------------------------------------------------------------------------
/*
    To copy an object in C++ means to call its:
    - copy constructor (if the object was not constructed before)
    or
    - the copy assignment operator (if the object was already constructed)

    If we do not provide our own implementation of these methods, then the
    default behavior is invoked.

    By default, all objects in C++ are copied "memberwise" ("shallow copy"),
    unless we define a copy constructor/copy assignment operator that overrides
    this behavior.
   
    Once we provide a copy constructor, we must also provide the zero arg
    constructor (if it's needed). The compiler generates the zero arg
    constructor only when no other constructor is explicitly defined (it's
    kind of a safety measure - https://stackoverflow.com/questions/11792207).

    The opposite is not true - if we explicitly define e.g. the default
    constructor, then the compiler will still generate the default copy
    contructor.

    All the STL containers have defined copy constructors which correctly copy
    the content of the STL objects.
*/
//------------------------------------------------------------------------------
class A
{
    public:
        // default constructor looks like this:
        // A () {}
        
        // default copy constructor looks like this:
        // A ( A const & obj )
        // {
        //     v_ = obj . v_;
        // }
        
        std::vector<int> v_;
};
//------------------------------------------------------------------------------
int main ()
{
    // default (zero-arg) constructor
    A a;

    a . v_ . push_back ( 5 );

    // copy initialization => default copy constructor is invoked
    A b = a;

    std::cout << a . v_ [0] << std::endl;    // 5
    std::cout << b . v_ [0] << std::endl;    // 5

    b . v_ [0] = 10;

    std::cout << a . v_ [0] << std::endl;    // 5
    std::cout << b . v_ [0] << std::endl;    // 10
    
    return 0;
}
